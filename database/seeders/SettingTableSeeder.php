<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting')->insert([
            'id_setting' => 1,
            'nama_perusahaan' => 'Toko Ijo',
            'alamat' => 'Jl. kadet swoko ds.pucangro',
            'telepon' => '085704702448',
            'tipe_nota' => 1, // kecil
            'diskon' => 5,
            'path_logo' => asset('/img/logo.png'),
            'path_kartu_member' => asset('/img/member.png'),
        ]);
    }
}
